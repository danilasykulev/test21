﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Test21
{

   
    public partial class MainForm : Form
    {


        private DataSet _ds;
        private SqlDataAdapter _adapter;
        private readonly string _connectionString; //У тебя строка подключения присутствует в app.config, соответственно оттуда ее и можно получать  @"Data Source=(localdb)\mssqllocaldb;Initial Catalog=Spr;Integrated Security=True"; // расположение базы данных
        private const string SelectAllQuery = "SELECT * FROM tab7";
        private SqlConnection _sqlConnection;                   //переменная типа sqlConnection


        public MainForm()
        {
            InitializeComponent();
            comboBox1.SelectedIndex = 0;                       // устанавливаем значение пол муж. в comboBox1
            _connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Test21.Properties.Settings.sprConnectionString"].ConnectionString;
            ReloadTableFromDatabase();

        }


        private void MainFormLoad(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "sprDataSet.Tab7". При необходимости она может быть перемещена или удалена.
            this.tab7TableAdapter.Fill(this.sprDataSet.Tab7);

            CalculateAge();
        }


        private void CalculateAge() // расчет возраста
        {

            DateTime currentDate = DateTime.Now;

            for (int i = 0; i < tab7DataGridView.Rows.Count - 1; i++)
            {
                var dBirth = tab7DataGridView.Rows[i].Cells[5].Value.ToString();
                if (dBirth != "")
                {
                    DateTime war = DateTime.Parse((Convert.ToString(dBirth)));

                    TimeSpan span = currentDate - war;
                    int age = (span.Days) / 365;

                    tab7DataGridView.Rows[i].Cells[6].Value = age;
                }
                else
                {
                    int age = 0;
                    tab7DataGridView.Rows[i].Cells[6].Value = age;
                }

            }


        }


        private void ReloadTableFromDatabase()                   // Соединение с БД, формирование таблицы, отображение данных
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                _adapter = new SqlDataAdapter(SelectAllQuery, connection);             // Создаем объект DataAdapter

                _ds = new DataSet();                                // Создаем объект Dataset
                _adapter.Fill(_ds);                               // Заполняем Dataset
                tab7DataGridView.DataSource = _ds.Tables[0];        // Отображаем данные

                tab7DataGridView.Columns[0].ReadOnly = true;       // делаем недоступным столбец id для изменения
            }

        }





        private void OnWriteToDatabaseButtonClick(object sender, EventArgs e) //кнопка запись в БД
        {
            try
            {
                _sqlConnection = new SqlConnection(_connectionString);


                //TODO В данном случае ты создаешь новый объект connection, и хочешь чтобы после его использования он освободил память (вызов Dispose у IDisposable)
                //TODO Подход правильный, но проблема в том, что используешь то ты: _sqlConnection, соответственно его и нужно помещать в using!
                //TODO Сделай _sqlConnection локальной переменной в рамках using-a и избавься от глобальной переменой _sqlConnection. Незачем держать открытым соединение к БД на этапе работы всего приложения. (Как в твоем методе SetConn или OnSaveChangesClick)
                using (System.Data.IDbConnection connection = new SqlConnection(_connectionString))
                {

                    _sqlConnection.Open();


                    if (!string.IsNullOrEmpty(mTBFam.Text) && !string.IsNullOrWhiteSpace(mTBFam.Text))

                    {
                        SqlCommand command = new SqlCommand("INSERT INTO [tab7] (FirstName, LastName, MiddlName, Sex, DateBirth) VALUES" +
                            " (@FirstName, @LastName, @MiddlName, @Sex, @DateBirth)", _sqlConnection);



                        command.Parameters.AddWithValue("FirstName", mTBFam.Text);  //вносим данные из боксов в базу
                        command.Parameters.AddWithValue("LastName", mTBName.Text);
                        command.Parameters.AddWithValue("MiddlName", mTBOtch.Text);
                        if (comboBox1.SelectedIndex == 0)
                        {
                            command.Parameters.AddWithValue("Sex", "муж");
                        }
                        if (comboBox1.SelectedIndex == 1)
                        {
                            command.Parameters.AddWithValue("Sex", "жен");
                        }

                        command.Parameters.AddWithValue("DateBirth", dateTimePicker1.Value);

                        command.ExecuteNonQuery();

                        _sqlConnection.Close();


                        mTBFam.Clear(); //очищаем боксы ввода
                        mTBName.Clear();
                        mTBOtch.Clear();

                    }

                    else
                    {
                        MessageBox.Show("Введите фамилию");
                    }

                    ReloadTableFromDatabase();


                    this.tab7TableAdapter.Fill(this.sprDataSet.Tab7);  // обновление DataGridView
                    CalculateAge();

                    tab7DataGridView.CurrentCell = tab7DataGridView[0, tab7DataGridView.Rows.Count - 2]; // выделение добавленной строки

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), ex.Source.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }



        private void OnSaveChangesClick(object sender, EventArgs e) //кнопка сохранить изменения
        {
            try
            {

                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    _adapter = new SqlDataAdapter(SelectAllQuery, connection);
                    new SqlCommandBuilder(_adapter);
                    _adapter.InsertCommand = new SqlCommand("sp_CreateSpr", connection);
                    _adapter.InsertCommand.CommandType = CommandType.StoredProcedure;
                    _adapter.InsertCommand.Parameters.Add(new SqlParameter("@FirstName", SqlDbType.NVarChar, 50, "FirstName"));
                    _adapter.InsertCommand.Parameters.Add(new SqlParameter("@LastName", SqlDbType.NVarChar, 50, "LastName"));
                    _adapter.InsertCommand.Parameters.Add(new SqlParameter("@MiddlName", SqlDbType.NVarChar, 50, "MiddlName"));
                    _adapter.InsertCommand.Parameters.Add(new SqlParameter("@Sex", SqlDbType.NVarChar, 3, "Sex"));
                    _adapter.InsertCommand.Parameters.Add(new SqlParameter("@DateBirth", SqlDbType.Date, 20, "DateBirth"));


                    SqlParameter parameter = _adapter.InsertCommand.Parameters.Add("@ID", SqlDbType.Int, 0, "ID");
                    parameter.Direction = ParameterDirection.Output;

                    _adapter.Update(_ds);

                    connection.Close();
                }
                CalculateAge();

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), ex.Source.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
            
        }




        private void butDelete_Click(object sender, EventArgs e) //кнопка удалить запись
        {
            foreach (DataGridViewRow row in tab7DataGridView.SelectedRows)
            {
                tab7DataGridView.Rows.Remove(row);
            }

        }




        private void textBox1_TextChanged(object sender, EventArgs e) // фильтр по фамилии
        {

            (tab7DataGridView.DataSource as DataTable).DefaultView.RowFilter =
         String.Format("FirstName like '{0}%'", textBox1.Text);
            CalculateAge();
        }



        private void Form1_FormClosing(object sender, FormClosingEventArgs e) // событие закрытия формы
        {
            if (_sqlConnection != null && _sqlConnection.State != ConnectionState.Closed)
                _sqlConnection.Close();
        }



        private void button1_Click(object sender, EventArgs e) //кнопка поиск значения по БД
        {
            {
                int k = 0;

                for (int i = 0; i < tab7DataGridView.RowCount; i++)
                {
                    tab7DataGridView.Rows[i].Selected = false;
                    for (int j = 0; j < tab7DataGridView.ColumnCount; j++)
                        if (tab7DataGridView.Rows[i].Cells[j].Value != null)

                            if (tab7DataGridView.Rows[i].Cells[j].Value.ToString().Contains(textBoxSeek.Text))

                            {
                                tab7DataGridView.Rows[i].Selected = true;
                                k = k+1;
                            }

                    if (i == tab7DataGridView.RowCount-1 & k == 0)
                    {
                        MessageBox.Show("Не найдено");

                    }

                    else
                    {
                        label2.Visible = true;
                        label2.Text = "найдено совпадений  "+k;
                    }
                }

            }

        }



        private void label2_Click(object sender, EventArgs e) //выключение сообщения о количестве найденых совпадений
        {
            label2.Visible = false;
        }
        


        private void bindingNavigatorMoveNextItem_Click(object sender, EventArgs e) //кнопка переместить вперед по записям
        {
            try
            {

                int index = tab7DataGridView.CurrentRow.Index;

                if (index != -1)
                {
                    tab7DataGridView.CurrentCell = tab7DataGridView[1, index + 1];
                    tab7DataGridView.Rows[index + 1].Selected = true;
                }
            }
            catch (IndexOutOfRangeException)
            {

            }
            catch (ArgumentOutOfRangeException)
            {

            }


        }




        private void bindingNavigatorMovePreviousItem_Click(object sender, EventArgs e) //кнопка переместить назад по записям
        {
            try
            {

                int index = tab7DataGridView.CurrentRow.Index;

                if (index != -1)
                {
                    tab7DataGridView.CurrentCell = tab7DataGridView[1, index - 1];
                    tab7DataGridView.Rows[index - 1].Selected = true;
                }
            }
            catch (IndexOutOfRangeException)
            {

            }
            catch (ArgumentOutOfRangeException)
            {

            }
        }




        private void bindingNavigatorMoveLastItem_Click(object sender, EventArgs e) //кнопка переместить на последнюю запись
        {
            try
            {

                if (tab7DataGridView.Rows.Count > 0)
                {
                    int index = tab7DataGridView.RowCount;
                    tab7DataGridView.Rows[index - 2].Selected = true;
                    tab7DataGridView.FirstDisplayedCell = tab7DataGridView.Rows[index - 2].Cells[1];
                    tab7DataGridView.CurrentCell = tab7DataGridView.Rows[index - 2].Cells[1];
                }

            }
            catch (IndexOutOfRangeException)
            {

            }
            catch (ArgumentOutOfRangeException)
            {

            }
        }




        private void bindingNavigatorMoveFirstItem_Click(object sender, EventArgs e) //кнопка переместить на первую запись
        {
            try
            {

                if (tab7DataGridView.Rows.Count > 0)
                {
                    tab7DataGridView.Rows[0].Selected = true;
                    tab7DataGridView.FirstDisplayedCell = tab7DataGridView.Rows[0].Cells[1];
                    tab7DataGridView.CurrentCell = tab7DataGridView.Rows[0].Cells[1];
                }

            }
            catch (IndexOutOfRangeException)
            {

            }
            catch (ArgumentOutOfRangeException)
            {

            }
        }

        private void OnExitToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (_sqlConnection != null && _sqlConnection.State != ConnectionState.Closed)
                _sqlConnection.Close();
            
            this.Close();
        }
    }
}


